" editorconfig-vim
" - .editorconfig parser

" vim-sensible
" - make default setting for vim

" vimwiki
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]
let g:vimwiki_conceallevel = 0
